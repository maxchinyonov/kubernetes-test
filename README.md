Create 1 pod:
```
kubectl apply -f pod.yaml
```

Replicaset:
```
kubectl create -f replicaset
kubectl edit replicaset - open up the running configuration in text format(temporary file in memory)
kubectl scale replicaset myapp-replicaset --replicas=2 - scale replicas to 2 pods
```

Deployments:
```
kubectl create -f deployment.yaml 
kubectl create -f deployment2.yaml 
kubectl describe deployments
kubectl describe deployment myapp1-deployment 
kubectl get deployments
kubectl get pods
kubectl get all
```

Rolling update
```
kubectl create -f deployment.yaml --record

update:
kubectl apply -f deployment-definition.yaml
kubectl set image deployment/myapp-deployment nginx=nginx:1.9.1

kubectl rollout status deployment
kubectl rollout status deployments
kubectl rollout history deployment 

rollback:
kubectl rollout undo deployment/myapp-deloyment
```


Services
```
kubectl create -f deployment.yaml
kubectl create -f service-definition.yaml 
kubectl get services
minikube service myapp --url
```

